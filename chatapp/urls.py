"""chatapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from chat.views import signup,ShowFriends,ShowChats,AddFriends,Contactlist,AddParticipant,StartConversation
from bio.views import Showinfo ,Events, ParticularEvent
from rest_framework.authtoken import views

urlpatterns = [

   


    path('admin/', admin.site.urls),
    path('signup/',signup.as_view()),
    path('api-token-auth/',views.obtain_auth_token),
    
    path('ShowFriends/',ShowFriends.as_view()),
    path('ShowChats/',ShowChats.as_view()),
    path('AddFriends/',AddFriends.as_view()),
    path('ContactList/',Contactlist.as_view()),
    path('AddParticipant/',AddParticipant.as_view()),
    path('StartCoversation/',StartConversation.as_view()),
    
    path('ShowInfo/',Showinfo.as_view()),
    path('Events/',Events.as_view()),
    path('ParticulatEvent/',ParticularEvent.as_view())



]
