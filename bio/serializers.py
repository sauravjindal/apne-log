from rest_framework import serializers
from .models import HealthInformation, DeathInformation ,Places, Event
from django.contrib.auth.models import User


class HealthInformation_serializer(serializers.ModelSerializer):

    class Meta:
        model = HealthInformation
        fields = '__all__'



class DeathInformation_serializer(serializers.ModelSerializer):

    class Meta:
        model = DeathInformation
        fields = '__all__'


class Places_serializer(serializers.ModelSerializer):

    class Meta:
        model = Places
        fields = '__all__'

class User_serializer(serializers.ModelSerializer):
     class Meta:
         model=User
         fields = ('id','username')

class Event_serializer(serializers.ModelSerializer):
    joinies=User_serializer(many=True)

    class Meta:
        model = Event
        fields = '__all__'

