from django.contrib import admin
from .models import DeathInformation,HealthInformation,Places,Event,Post
# Register your models here.

admin.site.register(DeathInformation),
admin.site.register(Places),
admin.site.register(HealthInformation),
admin.site.register(Event),
admin.site.register(Post)