from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import HealthInformation,DeathInformation,Places,Event
from .serializers import DeathInformation_serializer, HealthInformation_serializer, Places_serializer,Event_serializer


class Showinfo(APIView):

    def get(self,request):
       
        health = HealthInformation.objects.filter(user=request.user).all()
        death = DeathInformation.objects.filter(user=request.user).all()
        place = Places.objects.filter(user=request.user).all()

        obj = HealthInformation_serializer(health,many=True)
        obj1 = DeathInformation_serializer(death,many=True)
        obj2 = Places_serializer(place ,many=True)

        return Response({
            "HealthInformation": obj.data , "DeathInformation":obj1.data , "Places": obj2.data
        })



    def post(self,request):
        if request.method=="POST":
            Place_of_birth = request.data['Place_of_birth']
            Place_of_death = request.data['Place_of_death']
            Current_address = request.data['Current_address']
            Family_address =  request.data['Family_address']


            age= request.data['age']
            gender= request.data['gender']
            medical_condition = request.data['medical_condition ']
            medical_health_condition = request.data[' medical_health_condition']
            alcoholism = request.data['alcoholism']
            addictions = request.data['addictions']
            pregnancy_complicaions = request.data['pregnancy_complicaions']
            miscarriage = request.data['miscarriage']
            birth_defects = request.data['birth_defects']
            infertility = request.data['infertility']


            death_date = request.data['death_date']
            reason_of_death = request.data['reason_of_death']
            autopsy_report = request.data['autopsy_report']
            age_at_death = request.data[' age_at_death']
    
            DeathInformation.objects.create(user=request.user,
            death_date=death_date,
            reason_of_death=reason_of_death,
            autopsy_report=autopsy_report,
            age_at_death=age_at_death)

            HealthInformation.objects.create(user=request.user,
            age=age,
            gender=gender,
            medical_condition=medical_condition,
            medical_health_condition=medical_health_condition,
            alcoholism=alcoholism,
            addictions=addictions,
            pregnancy_complicaions=pregnancy_complicaions,
            miscarriage=miscarriage,
            birth_defects=birth_defects,
            infertility=infertility)

            Places.objects.create(user=request.user,
            Place_of_birth=Place_of_birth,
            Place_of_death=Place_of_death,
            Current_address=Current_address,
            Family_address=Family_address)
            return Response({"Notification":"Data Added"})
            

class Events(APIView):

    def get(self,request):
        events = Event.objects.all()
        obj = Event_serializer(events,many=True)
        return Response({
            "Events":obj.data
        })

    def post(self,request):
        name = request.data['name']
        location = request.data[' location']
        date = request.data['date']
        time = request.data['time']
        organiser = request.data[' organiser']
        details = request.data['details']
        Event.objects.create(name=name,location=location,data=date,time=time,organiser=organiser,details=details)
        return Response({"Event":"Created"})

class ParticularEvent(APIView):
    
    def get(self,request):
        id=request.GET['id']
        event = Event.objects.filter(id=id).all()
        obj = Event_serializer(event,many=True)
        return Response({"Event Details":obj.data})

   
