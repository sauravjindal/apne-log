# Generated by Django 3.1 on 2020-09-16 17:08

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bio', '0003_event'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='joinies',
            field=models.ManyToManyField(blank=True, related_name='events', to=settings.AUTH_USER_MODEL),
        ),
    ]
