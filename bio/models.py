from django.db import models

# Create your models here.
from django.contrib.auth.models import User


class Places(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    Place_of_birth = models.CharField(max_length=50)
    Place_of_death = models.CharField(max_length=50)
    Current_address = models.CharField(max_length=50)
    Family_address = models.CharField(max_length=50)


class HealthInformation(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    age= models.CharField(max_length=2)
    gender= models.CharField(max_length=10)
    medical_condition = models.CharField(max_length=20)
    medical_health_condition = models.CharField(max_length=50)
    alcoholism= models.BooleanField(default=False)
    addictions = models.CharField(max_length=20)
    pregnancy_complicaions = models.CharField(max_length=10)
    miscarriage = models.CharField(max_length=10)
    birth_defects = models.CharField(max_length=20)
    infertility = models.CharField(max_length=10)

class DeathInformation(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    death_date = models.DateField()
    reason_of_death = models.CharField(max_length=10)
    autopsy_report = models.CharField(max_length=100)
    age_at_death = models.DateField()
    

class Event(models.Model):
    name = models.CharField(max_length=10)
    location = models.CharField(max_length=10)
    date = models.DateField()
    time = models.TimeField()
    organiser = models.CharField(max_length=50)
    details = models.CharField(max_length=50)
    joinies = models.ManyToManyField(User,related_name='events',blank=True)


class Post(models.Model):
    text = models.CharField(max_length=1500)
    image_url = models.ImageField()
    ts = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)