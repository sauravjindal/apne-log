from django.contrib import admin
from .models import Contact,Message,Conversation


admin.site.register(Contact),
admin.site.register(Message),
admin.site.register(Conversation),
# Register your models here.
