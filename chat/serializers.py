from rest_framework import serializers
from .models import Conversation,Contact,Message
from django.contrib.auth.models import User






class Contactlistserializer(serializers.ModelSerializer):
    user=serializers.StringRelatedField()
    class Meta:
        model = Contact
        fields=('id','user')


class Messageserializer(serializers.ModelSerializer):
    conact=serializers.StringRelatedField()

    class Meta:
        model=Message
        fields=('conact','content',)


class Friendslistserializer(serializers.ModelSerializer):
    friends=Contactlistserializer(many=True)

    class Meta:
        model=Contact
        fields=('friends',)

    
class Conversationserializer(serializers.ModelSerializer):
    participants=serializers.StringRelatedField(many=True)
    messages=Messageserializer(many=True)

    class Meta:
        model=Conversation
        fields='__all__'