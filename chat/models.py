from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.


class Contact(models.Model):
    user=models.ForeignKey(User,related_name='friends',on_delete=models.CASCADE)
    friends=models.ManyToManyField('self',blank=True)

    def __str__(self):
        return self.user.username

class Message(models.Model):
    conact=models.ForeignKey(Contact,related_name='messages',on_delete=models.CASCADE)
    content=models.TextField()
    timestamp=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.conact.user.username

class Conversation(models.Model):
    participants=models.ManyToManyField(Contact,related_name='coversations',blank=True)
    messages=models.ManyToManyField(Message,blank=True)


  



