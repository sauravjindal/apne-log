
from channels.db import database_sync_to_async


from django.contrib.auth.models import User
from.models import Conversation,Contact,Message
from.views import sender,current_conversation




from asgiref.sync import sync_to_async
import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer



class ChatConsumer(WebsocketConsumer):
    def connect(self):
        print("hello")
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    


    def new_message(self,data):
        sender_id=sender(data['sender_id'])
        current_convo=current_conversation(data['conversation_id'])

        message=Message.objects.create(
            conact=sender_id,
            content=data['text'],

        )

        current_convo.messages.add(message)
        current_convo.save()

        context ={
            
            "message":self.message_to_json(message)
        }
        return self.send_chat_message(context)

    def message_to_json(self,message):
        context={
            
            "from":message.conact.user.username,
            'id':message.conact.id,
            "content":message.content,
            "timestamp": str(message.timestamp),
        }
        return context


    method= {

        "new_message":new_message,
        
    }





    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        print("receive called")
        data = json.loads(text_data)
        self.method[data['method']](self,data)   #method recieve from frontend

        # Send message to room group
    def send_chat_message(self,message):
        async_to_sync(self.channel_layer.group_send)(
        self.room_group_name,
        {
            'type': 'chat_message',
            'message': message
        }
     )
        

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))









































