from django.shortcuts import render
from django.contrib.auth.models import User
from.models import Conversation,Contact,Message

from .serializers import Contactlistserializer,Friendslistserializer,Conversationserializer
from rest_framework.views  import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token




def sender(sender_name):
    sender_contact=Contact.objects.get(id=sender_name)
    return sender_contact



def current_conversation(conversation_id):
    convo=Conversation.objects.get(id=conversation_id)
    return convo









class signup(APIView):

    def post(self,request):
        username=request.data['username']
        password=request.data['password']
        user=User.objects.create_user(username=username,password=password)
        Contact.objects.create(user=user)
        Token.objects.create(user=user)
        token =Token.objects.get(user=user).key
        return Response({"Message":"User Created","Token":token})


class Contactlist(APIView):

    def get(self,request):
        obj=Contact.objects.all()
        users=Contactlistserializer(obj,many=True)
        return Response({
            "Contacts":users.data
        })
    

class AddFriends(APIView):

    def get(self,request):
        id=request.GET['id']
        friend=Contact.objects.get(id=id)
        obj=friend.user.username
        contact=Contact.objects.get(user=request.user)
        contact.friends.add(friend)
        return Response({
            "Friend Added Is":obj
        })
    

#Show friends Of a Contact#
class ShowFriends(APIView):

    def get(self,request):
       friends=Contact.objects.filter(user=request.user)
       #user=User.objects.get(username=requst.user)#
       #friends=user.friends.all()#
       friend=Friendslistserializer(friends,many=True)
       return Response({"Friends":friend.data})


#Show Chat of Authenticated User
class ShowChats(APIView):

    def get(self,request):
        contact=Contact.objects.get(user=request.user)
        conv=contact.coversations.all()
        #conv=Conversation.objects.filter(participants=contact)#
        obj=Conversationserializer(conv,many=True)
        return Response({"Conversation":obj.data})



#Add Participant to particular chat
class AddParticipant(APIView):

    def get(self,request):
        id=request.GET['id']
        conversation_id=request.GET['convid']
        contact=Contact.objects.get(id=id)
        conversation=Conversation.objects.get(id=conversation_id)
        conversation.participants.add(contact)
        return Response(
            {
                "Participant Added Is" : contact.user.username
            }
        )



class StartConversation(APIView):

     def get(self,request):
         id=request.GET['id']
         contact=Contact.objects.get(id=id)
         current = Contact.objects.get(user=request.user)
         conversation= Conversation.objects.create()
         conversation.participants.add(current,contact)
         return Response({
             "Conversation Started Between": current.user.username
         })




